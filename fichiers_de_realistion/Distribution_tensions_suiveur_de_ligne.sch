EESchema Schematic File Version 4
LIBS:Distribution_tensions_suiveur_de_ligne-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Carte distribution de tensions - Robot suiveur de ligne"
Date "2024-04-06"
Rev "1.2"
Comp "I.U.T. G.E.I.I. Brive-La-Gaillarde"
Comment1 "PATIENT"
Comment2 "PAUL"
Comment3 "DARTINSET"
Comment4 "USCHÉ"
$EndDescr
$Comp
L Connector_Generic:Conn_01x03 J6
U 1 1 66113806
P 8200 2700
F 0 "J6" H 8250 2900 50  0000 R CNN
F 1 "Bornier +12V" H 8800 2700 50  0000 R CNN
F 2 "Connector_JST:JST_NV_B03P-NV_1x03_P5.00mm_Vertical" H 8200 2700 50  0001 C CNN
F 3 "~" H 8200 2700 50  0001 C CNN
	1    8200 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 66113915
P 8000 2900
F 0 "#PWR0101" H 8000 2650 50  0001 C CNN
F 1 "GND" H 8005 2727 50  0000 C CNN
F 2 "" H 8000 2900 50  0001 C CNN
F 3 "" H 8000 2900 50  0001 C CNN
	1    8000 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+12V #PWR0102
U 1 1 6611473D
P 8000 2350
F 0 "#PWR0102" H 8000 2200 50  0001 C CNN
F 1 "+12V" H 8000 2500 50  0000 C CNN
F 2 "" H 8000 2350 50  0001 C CNN
F 3 "" H 8000 2350 50  0001 C CNN
	1    8000 2350
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 661163EB
P 7800 2800
F 0 "#FLG0101" H 7800 2875 50  0001 C CNN
F 1 "PWR_FLAG" H 7550 2850 50  0000 C CNN
F 2 "" H 7800 2800 50  0001 C CNN
F 3 "~" H 7800 2800 50  0001 C CNN
	1    7800 2800
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 66117433
P 7800 2600
F 0 "#FLG0102" H 7800 2675 50  0001 C CNN
F 1 "PWR_FLAG" H 7550 2650 50  0000 C CNN
F 2 "" H 7800 2600 50  0001 C CNN
F 3 "~" H 7800 2600 50  0001 C CNN
	1    7800 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J7
U 1 1 66118E12
P 9200 2700
F 0 "J7" H 9250 2900 50  0000 R CNN
F 1 "Bornier +12V" H 9800 2700 50  0001 R CNN
F 2 "Connector_JST:JST_NV_B03P-NV_1x03_P5.00mm_Vertical" H 9200 2700 50  0001 C CNN
F 3 "~" H 9200 2700 50  0001 C CNN
	1    9200 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 2800 9000 2700
Connection ~ 9000 2700
Wire Wire Line
	9000 2700 9000 2600
$Comp
L Device:LED D1
U 1 1 6611E74B
P 9000 3450
F 0 "D1" V 9000 3400 50  0000 R CNN
F 1 "LED_rouge" H 9150 3550 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 9000 3450 50  0001 C CNN
F 3 "~" H 9000 3450 50  0001 C CNN
	1    9000 3450
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R1
U 1 1 6611EFFB
P 9000 3050
F 0 "R1" H 9070 3088 50  0000 L CNN
F 1 "1K" H 9070 3005 39  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 8930 3050 50  0001 C CNN
F 3 "~" H 9000 3050 50  0001 C CNN
	1    9000 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9000 3300 9000 3200
Wire Wire Line
	9000 2900 9000 2800
Connection ~ 9000 2800
Wire Wire Line
	9000 3600 9000 3650
$Comp
L power:GND #PWR0104
U 1 1 66120495
P 9000 3650
F 0 "#PWR0104" H 9000 3400 50  0001 C CNN
F 1 "GND" H 9005 3477 50  0000 C CNN
F 2 "" H 9000 3650 50  0001 C CNN
F 3 "" H 9000 3650 50  0001 C CNN
	1    9000 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7800 2600 8000 2600
Connection ~ 8000 2600
Wire Wire Line
	8000 2600 8000 2700
Wire Wire Line
	8000 2900 8000 2800
Wire Wire Line
	7800 2800 8000 2800
Connection ~ 8000 2800
Connection ~ 5500 4950
Wire Wire Line
	5500 4950 6250 4950
$Comp
L power:GND #PWR01
U 1 1 661466F9
P 5500 4950
F 0 "#PWR01" H 5500 4700 50  0001 C CNN
F 1 "GND" H 5505 4777 50  0000 C CNN
F 2 "" H 5500 4950 50  0001 C CNN
F 3 "" H 5500 4950 50  0001 C CNN
	1    5500 4950
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR02
U 1 1 6614AEEB
P 5600 4150
F 0 "#PWR02" H 5600 4000 50  0001 C CNN
F 1 "+5V" H 5615 4323 50  0000 C CNN
F 2 "" H 5600 4150 50  0001 C CNN
F 3 "" H 5600 4150 50  0001 C CNN
	1    5600 4150
	1    0    0    -1  
$EndComp
Connection ~ 5600 4150
Wire Wire Line
	5600 4150 6350 4150
Wire Wire Line
	4850 3900 5700 3900
Wire Wire Line
	5700 3900 6450 3900
Connection ~ 5700 3900
Text Label 5000 4900 1    39   ~ 0
A0
Text Label 5000 4200 3    39   ~ 0
A1
Text Label 5850 4900 1    39   ~ 0
A2
Text Label 5850 4200 3    39   ~ 0
A3
Text Label 6600 4900 1    39   ~ 0
A4
Text Label 6600 4200 3    39   ~ 0
A5
Wire Notes Line
	7350 5050 8050 5050
Wire Notes Line
	7350 4050 8050 4050
Wire Notes Line
	7350 4050 7350 5050
Wire Notes Line
	8050 4050 8050 5050
Text Notes 7500 5150 0    39   ~ 8
Jumpers GND
Text Notes 7500 4000 0    39   ~ 8
Jumpers +5V
Wire Notes Line
	5550 2700 5550 2300
Text Notes 5550 2250 0    39   ~ 8
Jumpers CTRL
Text Notes 5250 5300 0    39   ~ 8
Serie de jumpers -> capteurs
$Comp
L Device:LED D2
U 1 1 6619F25C
P 6850 2700
F 0 "D2" H 6950 2600 50  0000 R CNN
F 1 "LED_verte" H 6950 2800 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 6850 2700 50  0001 C CNN
F 3 "~" H 6850 2700 50  0001 C CNN
	1    6850 2700
	-1   0    0    1   
$EndComp
$Comp
L Device:R R2
U 1 1 661A00F3
P 6450 2700
F 0 "R2" V 6550 2700 50  0000 L CNN
F 1 "330" V 6550 2550 39  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6380 2700 50  0001 C CNN
F 3 "~" H 6450 2700 50  0001 C CNN
	1    6450 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6600 2700 6700 2700
Wire Wire Line
	7000 2700 7050 2700
Wire Wire Line
	5650 2650 5700 2650
Connection ~ 5700 2650
Wire Wire Line
	5700 2650 5750 2650
Wire Wire Line
	4750 4150 5600 4150
Wire Wire Line
	4650 4950 5500 4950
$Comp
L power:GND #PWR0103
U 1 1 661B2211
P 7050 2700
F 0 "#PWR0103" H 7050 2450 50  0001 C CNN
F 1 "GND" H 7055 2527 50  0000 C CNN
F 2 "" H 7050 2700 50  0001 C CNN
F 3 "" H 7050 2700 50  0001 C CNN
	1    7050 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5700 2650 5700 3900
Text Notes 6250 2550 0    31   ~ 6
Signalisation du lancement du programme
$Comp
L Device:LED D3
U 1 1 661BAAE9
P 3350 4700
F 0 "D3" V 3350 4650 50  0000 R CNN
F 1 "LED_rouge" H 3450 4800 50  0000 R CNN
F 2 "LED_THT:LED_D3.0mm" H 3350 4700 50  0001 C CNN
F 3 "~" H 3350 4700 50  0001 C CNN
	1    3350 4700
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 661BAAF0
P 3350 4300
F 0 "R3" H 3420 4338 50  0000 L CNN
F 1 "330" H 3420 4255 39  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 3280 4300 50  0001 C CNN
F 3 "~" H 3350 4300 50  0001 C CNN
	1    3350 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 4450 3350 4550
Wire Wire Line
	3350 4850 3350 4900
$Comp
L power:GND #PWR0105
U 1 1 661BAAF9
P 3350 4900
F 0 "#PWR0105" H 3350 4650 50  0001 C CNN
F 1 "GND" H 3355 4727 50  0000 C CNN
F 2 "" H 3350 4900 50  0001 C CNN
F 3 "" H 3350 4900 50  0001 C CNN
	1    3350 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0106
U 1 1 661BBCFE
P 3350 3800
F 0 "#PWR0106" H 3350 3650 50  0001 C CNN
F 1 "+5V" H 3365 3973 50  0000 C CNN
F 2 "" H 3350 3800 50  0001 C CNN
F 3 "" H 3350 3800 50  0001 C CNN
	1    3350 3800
	1    0    0    -1  
$EndComp
Text Notes 3150 3700 3    39   ~ 8
Signalisation de l'alimentation des capteurs
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 661C9917
P 3500 3800
F 0 "#FLG0103" H 3500 3875 50  0001 C CNN
F 1 "PWR_FLAG" H 3750 3850 50  0000 C CNN
F 2 "" H 3500 3800 50  0001 C CNN
F 3 "~" H 3500 3800 50  0001 C CNN
	1    3500 3800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8000 2350 8000 2600
Wire Bus Line
	9000 2600 9000 2400
Wire Bus Line
	8650 2400 8400 2250
Wire Bus Line
	8650 2400 9000 2400
Wire Bus Line
	8400 2400 8000 2400
Wire Notes Line
	7850 2150 9300 2150
Wire Notes Line
	9300 2150 9300 3850
Wire Notes Line
	9300 3850 7850 3850
Wire Notes Line
	7850 2150 7850 3850
Text Notes 7900 2100 0    39   ~ 8
Borniers réception et alimentation en +12 V
Wire Notes Line
	9300 2900 8850 2900
Wire Notes Line
	8850 2900 8850 3850
Text Notes 8800 3050 3    39   ~ 8
Signalisation du +12V
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 661F603C
P 4850 4600
F 0 "J1" V 4950 4300 50  0000 C CNN
F 1 "Capteurs de gauche" V 4900 4600 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4850 4600 50  0001 C CNN
F 3 "~" H 4850 4600 50  0001 C CNN
	1    4850 4600
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 661F60A2
P 5700 4600
F 0 "J2" V 5800 4300 50  0000 C CNN
F 1 "Capteurs centraux" V 5750 4600 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 5700 4600 50  0001 C CNN
F 3 "~" H 5700 4600 50  0001 C CNN
	1    5700 4600
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 661F60D4
P 6450 4600
F 0 "J3" V 6550 4300 50  0000 C CNN
F 1 "Capteurs de droite" V 6500 4600 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 6450 4600 50  0001 C CNN
F 3 "~" H 6450 4600 50  0001 C CNN
	1    6450 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4650 4800 4650 4300
Wire Wire Line
	4650 4950 4650 4800
Connection ~ 4650 4800
Wire Wire Line
	4750 4800 4750 4300
Wire Wire Line
	4750 4150 4750 4300
Connection ~ 4750 4300
Wire Wire Line
	4850 4800 4850 4300
Wire Wire Line
	4850 3900 4850 4300
Connection ~ 4850 4300
Wire Wire Line
	4950 4800 5000 4800
Wire Wire Line
	4950 4300 5000 4300
Wire Wire Line
	5000 4800 5000 4900
Connection ~ 5000 4800
Wire Wire Line
	5000 4800 5050 4800
Wire Wire Line
	5000 4300 5000 4200
Connection ~ 5000 4300
Wire Wire Line
	5000 4300 5050 4300
Wire Wire Line
	5600 4800 5600 4300
Wire Wire Line
	5600 4300 5600 4150
Connection ~ 5600 4300
Wire Wire Line
	5500 4300 5500 4800
Wire Wire Line
	5500 4800 5500 4950
Connection ~ 5500 4800
Wire Wire Line
	5700 4800 5700 4300
Wire Wire Line
	5700 4300 5700 3900
Connection ~ 5700 4300
Wire Wire Line
	5800 4300 5850 4300
Wire Wire Line
	5850 4300 5850 4200
Connection ~ 5850 4300
Wire Wire Line
	5850 4300 5900 4300
Wire Wire Line
	5800 4800 5850 4800
Wire Wire Line
	5850 4800 5850 4900
Connection ~ 5850 4800
Wire Wire Line
	5850 4800 5900 4800
Wire Wire Line
	6250 4800 6250 4300
Wire Wire Line
	6250 4950 6250 4800
Connection ~ 6250 4800
Wire Wire Line
	6350 4800 6350 4300
Wire Wire Line
	6350 4300 6350 4150
Connection ~ 6350 4300
Wire Wire Line
	6450 4800 6450 4300
Wire Wire Line
	6450 4300 6450 3900
Connection ~ 6450 4300
Wire Wire Line
	6550 4800 6600 4800
Wire Wire Line
	6600 4800 6600 4900
Connection ~ 6600 4800
Wire Wire Line
	6600 4800 6650 4800
Wire Wire Line
	6650 4300 6600 4300
Wire Wire Line
	6600 4300 6600 4200
Connection ~ 6600 4300
Wire Wire Line
	6600 4300 6550 4300
Wire Notes Line
	4450 3800 4450 5200
Wire Notes Line
	6850 3800 6850 5200
Wire Notes Line
	4450 5200 6850 5200
Wire Notes Line
	3200 3550 3200 5150
Wire Wire Line
	3350 3800 3500 3800
Connection ~ 3350 3800
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 66239FE9
P 3550 3950
F 0 "J8" H 3629 3896 50  0000 L CNN
F 1 "Conn_01x02" H 3630 3851 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 3550 3950 50  0001 C CNN
F 3 "~" H 3550 3950 50  0001 C CNN
	1    3550 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 4150 3350 4050
Wire Wire Line
	3350 3800 3350 3950
Wire Notes Line
	3650 3550 3650 5150
Wire Notes Line
	3200 3550 3650 3550
Wire Notes Line
	3200 5150 3650 5150
$Comp
L Connector_Generic:Conn_01x06 J4
U 1 1 66146705
P 5850 2450
F 0 "J4" V 5950 2450 50  0000 R CNN
F 1 "Conn_01x06" V 5725 2062 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 5850 2450 50  0001 C CNN
F 3 "~" H 5850 2450 50  0001 C CNN
	1    5850 2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5850 2650 5950 2650
Wire Wire Line
	6300 2700 6150 2700
Wire Notes Line
	5550 2700 6000 2700
Wire Notes Line
	6000 2700 6000 2300
$Comp
L Switch:SW_SPST SW1
U 1 1 6616297E
P 6350 2900
F 0 "SW1" H 6350 2800 50  0000 C CNN
F 1 "SW_SPST" H 6350 3000 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 6350 2900 50  0001 C CNN
F 3 "" H 6350 2900 50  0001 C CNN
	1    6350 2900
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 66167913
P 6700 2900
F 0 "#PWR0107" H 6700 2750 50  0001 C CNN
F 1 "+5V" H 6600 2950 50  0000 C CNN
F 2 "" H 6700 2900 50  0001 C CNN
F 3 "" H 6700 2900 50  0001 C CNN
	1    6700 2900
	1    0    0    -1  
$EndComp
Wire Wire Line
	6150 2650 6150 2700
$Comp
L Device:R R4
U 1 1 6617523E
P 6050 3150
F 0 "R4" H 6120 3188 50  0000 L CNN
F 1 "10K" H 6120 3105 39  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5980 3150 50  0001 C CNN
F 3 "~" H 6050 3150 50  0001 C CNN
	1    6050 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 3300 6050 3500
$Comp
L power:GND #PWR0108
U 1 1 6617713E
P 6050 3500
F 0 "#PWR0108" H 6050 3250 50  0001 C CNN
F 1 "GND" H 6055 3327 50  0000 C CNN
F 2 "" H 6050 3500 50  0001 C CNN
F 3 "" H 6050 3500 50  0001 C CNN
	1    6050 3500
	1    0    0    -1  
$EndComp
Wire Notes Line
	5550 2300 7300 2300
Text Notes 6250 2250 0    39   ~ 8
Switch démarrage du programme
Wire Notes Line
	4450 3800 7300 3800
Wire Notes Line
	7300 2300 7300 3800
$Comp
L Connector_Generic:Conn_01x05 J5
U 1 1 66156616
P 7700 4400
F 0 "J5" V 7700 4750 50  0000 R CNN
F 1 "Jumpers +5V" V 7575 4112 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 7700 4400 50  0001 C CNN
F 3 "~" H 7700 4400 50  0001 C CNN
	1    7700 4400
	0    1    1    0   
$EndComp
Connection ~ 7700 4200
$Comp
L power:+5V #PWR0109
U 1 1 6616560D
P 7700 4200
F 0 "#PWR0109" H 7700 4050 50  0001 C CNN
F 1 "+5V" H 7715 4373 50  0000 C CNN
F 2 "" H 7700 4200 50  0001 C CNN
F 3 "" H 7700 4200 50  0001 C CNN
	1    7700 4200
	1    0    0    -1  
$EndComp
Connection ~ 7800 4200
Wire Wire Line
	7800 4200 7900 4200
Wire Wire Line
	7700 4200 7800 4200
Connection ~ 7600 4200
Wire Wire Line
	7600 4200 7700 4200
Wire Wire Line
	7500 4200 7600 4200
$Comp
L Connector_Generic:Conn_01x05 J9
U 1 1 66167674
P 7700 4600
F 0 "J9" V 7700 4350 50  0000 R CNN
F 1 "Jumpers GND" V 7575 4312 50  0001 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 7700 4600 50  0001 C CNN
F 3 "~" H 7700 4600 50  0001 C CNN
	1    7700 4600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7500 4800 7600 4800
Connection ~ 7600 4800
Wire Wire Line
	7600 4800 7700 4800
Connection ~ 7700 4800
Wire Wire Line
	7700 4800 7800 4800
Connection ~ 7800 4800
Wire Wire Line
	7800 4800 7900 4800
$Comp
L power:GND #PWR0110
U 1 1 6616985C
P 7700 4800
F 0 "#PWR0110" H 7700 4550 50  0001 C CNN
F 1 "GND" H 7705 4627 50  0000 C CNN
F 2 "" H 7700 4800 50  0001 C CNN
F 3 "" H 7700 4800 50  0001 C CNN
	1    7700 4800
	1    0    0    -1  
$EndComp
Wire Notes Line
	7350 4500 8050 4500
Wire Wire Line
	6050 2650 6050 2900
Wire Wire Line
	6050 2900 6150 2900
Connection ~ 6050 2900
Wire Wire Line
	6050 2900 6050 3000
Wire Wire Line
	6550 2900 6700 2900
$EndSCHEMATC
